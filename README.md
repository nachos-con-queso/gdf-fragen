# README

Dieses Bitbucket-Projekt existiert hauptsächlich, damit jeder die Änderungen am Dokument nachvollziehen kann.

Dazu einfach links auf `Commits` klicken. Dort sieht man dann die Liste der letzten Commits und kann sich mit Klick auf den Commit-Hash die Änderungen zum jeweils vorhergehenden Commit anschauen.

Die Datei `main.tex` kann und darf heruntergeladen werden und ist für sich kompilierfähig. Da hier das Latex-Package `gitinfo2` verwendet wird, empfieht es sich, den entsprechenden `\usepackage`-Befehl zu löschen bzw. auszukommentieren. Zudem macht es Sinn, die Datei erst nach dem 02.07.2018 herunterzuladen (sofern Zugriff auf das jeweils aktuelle PDF besteht), denn bis dahin ist das Dokument under heavy development.
